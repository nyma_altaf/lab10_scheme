(define ADD (lambda (m n s z) (m s (n s z))))

(define SUBTRACT (lambda (m n) (n pred m)))

(define AND (lambda (m n) (lambda (a b) (n (m a b) b))))

(define OR (lambda (m n) (lambda (a b) (n a (m a b)))))

(define NOT (lambda (m) (lambda (a b) (m b a))))

(define IsZero (lambda (n) (n (lambda (x) (false)) true)))

(define LEQ (lambda (m n) (IsZero (SUBTRACT m n))))

(define GEQ (lambda (m n) (NOT (LT mn))))
